package c.facci.richardgarcia.practica;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Calendar;

public class Main2Activity extends AppCompatActivity {

    Calendar calendario = Calendar.getInstance();
    TextView textViewMensaje;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        textViewMensaje = (TextView) findViewById(R.id.textViewMensaje);
        String dato = getIntent().getExtras().getString("valor");
        int semana = Integer.parseInt(dato);
        int datoIntroducido = calendario.get(Calendar.WEEK_OF_YEAR);

        if (datoIntroducido==semana ){
            textViewMensaje.setText("HAS ACERTADO");
        }else{
            textViewMensaje.setText("INTENTE NUEVAMENTE");
        }
    }
}
