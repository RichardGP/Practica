package c.facci.richardgarcia.practica;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText editTextIngreso;
    Button buttonRevisar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextIngreso = (EditText) findViewById(R.id.editTextIngreso);
        buttonRevisar = (Button) findViewById(R.id.buttonRevisar);
        editTextIngreso.setError(null);
        buttonRevisar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ingreso = editTextIngreso.getText().toString();
                boolean cancel = false;
                View focusView = null;

                if (TextUtils.isEmpty(ingreso)){
                    editTextIngreso.setError("Introduce el numero de la semana actual");
                    focusView = editTextIngreso;
                    cancel = true;
                }else{
                    Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                    intent.putExtra("valor", editTextIngreso.getText().toString());
                    startActivity(intent);
                }

            }
        });

    }
}
